volume_level = data.get("volume_level", 0.3)
message = data.get("message")
url = data.get("url")
interrupt_playback = data.get("interrupt_playback", False)
ignore_muted = data.get("ignore_muted", True)

prev_volumes = {}

def flatten(entity_id, result=[], searched=[]):
    state = hass.states.get(entity_id)

    if state is not None:
        entity_ids = state.attributes.get("entity_id")
        if entity_ids is not None:
            searched.append(entity_id)
            for entity_id in entity_ids:
                if entity_id not in result and entity_id not in searched:
                    flatten(entity_id, result, searched)
        else:
            result.append(entity_id)

def should_interrupt_playback(entity_id):
    if not interrupt_playback and hass.states.get(entity_id).state == "playing":
        logger.info("preventing playback interruption on %s", entity_id)
        return False
    return True

def is_available(entity_id):
    if hass.states.get(entity_id) == "unavailable":
        logger.info("%s entity is unavailable, skipping...", entity_id)
        return False
    return True

def is_unmuted(entity_id):
    if hass.states.get(entity_id).attributes["volume_level"] == 0 or hass.states.get(entity_id).attributes["is_volume_muted"] == True:
        logger.info("volume level is zero or device is muted for %s, skipping...", entity_id)
        logger.info("turning off %s", entity_id)
        hass.services.call("media_player", "turn_off", {"entity_id": entity_id}, True)
        return False
    return True

def wait_until_state(entities, state, timeout=10):
    start = time.time()
    for entity_id in entities:
        waited_time = 0
        while not hass.states.get(entity_id).state == state and time.time() - start < timeout:
            time.sleep(0.1)
            waited_time = waited_time + 0.1
        logger.info("waited %f seconds for %s until %s state", waited_time, entity_id, state)

def wait_until_volume_set(entities, timeout=10):
    start = time.time()
    for entity_id in entities:
        waited_time = 0
        while not abs(hass.states.get(entity_id).attributes["volume_level"] - volume_level) <= 0.05 and time.time() - start < timeout:
            time.sleep(0.1)
            waited_time = waited_time + 0.1
        logger.info("waited %f seconds for %s volume set", waited_time, entity_id)

logger.info("--- starting google_home notification script with data : %s ---", data)

if data.get("entity_id") is not None and (data.get("message") is not None or data.get("url") is not None):
    entities = []
    flatten(data.get("entity_id"), entities)

    entities = [entity_id for entity_id in entities if is_available(entity_id)]

    entities = [entity_id for entity_id in entities if should_interrupt_playback(entity_id)]

    logger.info("turning on %s", entities)
    hass.services.call("media_player", "turn_on", {"entity_id": entities}, True)

    wait_until_state(entities, "idle")
    time.sleep(0.5)

    if ignore_muted:
        entities = [entity_id for entity_id in entities if is_unmuted(entity_id)]

    for entity_id in entities:
        logger.info("getting current volume level for %s", entity_id)
        prev_volumes[entity_id] = hass.states.get(entity_id).attributes["volume_level"]

    if len(entities) == 0:
        logger.error("no entities defined")
        exit

    logger.info("setting TTS volume level to %f for %s", volume_level, entities)
    hass.services.call("media_player", "volume_set", {"entity_id": entities, "volume_level": volume_level}, False)
    wait_until_volume_set(entities)

    if data.get("message") is not None:
        logger.info("broadcasting %s message on %s", message, entities)
        hass.services.call("tts", "google_translate_say", {"entity_id": entities, "message": message}, True)
#         time.sleep(3) # Wait for playing state
        wait_until_state(entities, "idle")
    elif data.get("url") is not None:
        logger.info("playing %s url on %s", url, entities)
        hass.services.call("media_player", "play_media", {"entity_id": entities, "media_content_id": url, "media_content_type": "music"}, True)
#         time.sleep(2) # Wait for playing state
        wait_until_state(entities, "idle", 120)

    for entity_id in entities:
        logger.info("resetting volume level to %f on %s", prev_volumes[entity_id], entity_id)
        hass.services.call("media_player", "volume_set", {"entity_id": entity_id, "volume_level": prev_volumes[entity_id]}, True)

#     time.sleep(3)

    logger.info("turning off %s", entities)
    hass.services.call("media_player", "turn_off", {"entity_id": entities}, True)

logger.info("--- end of google_home notification script ---")
