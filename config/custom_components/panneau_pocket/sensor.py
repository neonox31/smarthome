"""Platform for sensor integration."""
from __future__ import annotations

from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntity,
    SensorStateClass,
)
from homeassistant.core import HomeAssistant
from homeassistant.components.sensor import ENTITY_ID_FORMAT
from homeassistant.const import CONF_NAME
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from homeassistant.helpers.entity import async_generate_entity_id

from custom_components.panneau_pocket.panneau_pocket import PanneauPocket
from custom_components.panneau_pocket.const import CONF_CITY

def setup_platform(
    hass: HomeAssistant,
    config: ConfigType,
    add_entities: AddEntitiesCallback,
    discovery_info: DiscoveryInfoType | None = None
) -> None:
    """Set up the sensor platform."""
    entity_id = async_generate_entity_id(ENTITY_ID_FORMAT, config.get(CONF_NAME), hass=hass)
    name = config.get(CONF_NAME)
    city = config.get(CONF_CITY)
    add_entities([PanneauPocketSensor(hass, entity_id, name, city)])


class PanneauPocketSensor(RestoreEntity):
    """Representation of a Sensor."""

    def __init__(self, hass, entity_id, name, city):
        self._hass = hass
        self.entity_id = entity_id
        self.name = name
        self.city = city
        self._pp = PanneauPocket(hass, city)
        self._state = None
        self.attrs = {}

    @property
    def name(self):
        """Return the name of the sensor."""
        return 'City News'

    @property
    def state(self):
        """Return the state of the sensor."""
        # if self._state is None:
        #     if self.attrs:
        #         if self.attrs['expired']:
        #             self._state = 0
        #         else:
        #             self._state = self.attrs['remaining']
        return self._state

    @property
    def device_state_attributes(self):
        return self.attrs

    async def async_update(self) -> None:
        """Get the latest data and updates the state."""
        await self._pp.async_update()

    async def async_added_to_hass(self) -> None:
        """Handle entity which will be added."""
        await super().async_added_to_hass()
        state = await self.async_get_last_state()
        if not state:
            return
        self._state = state.state

        async_dispatcher_connect(
            self._hass, DATA_UPDATED, self._schedule_immediate_update
        )

    @callback
    def _schedule_immediate_update(self):
        self.async_schedule_update_ha_state(True)
