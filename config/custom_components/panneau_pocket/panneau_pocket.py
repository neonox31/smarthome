import logging
import httpx
import re

from bs4 import BeautifulSoup
from homeassistant.helpers.httpx_client import get_async_client

_LOGGER = logging.getLogger(__name__)


class PanneauPocket:
    """Class for handling panneau pocket interactions."""

    def __init__(
        self,
        hass,
        city
    ):
        """Initialize the data object."""
        self._hass = hass
        self._url = f"https://app.panneaupocket.com/ville/{city}"
        self._timeout = 10
        self._async_client = None

    async def async_update(self, log_errors=True):
        """Get the latest data from REST service with provided method."""
        if not self._async_client:
            self._async_client = get_async_client(
                self._hass
            )
        try:
            page_content = await self._async_get_page_content()
            soup = BeautifulSoup(page_content, 'html.parser')
            news = soup.findAll("div", {"class": "sign-carousel--item"})
            for new in news:
                id = new['data-id']
                url = f"{self._url}?panneau={id}"
                title = new.findChild("div", {"class": "title"}).text.strip()
                content = new.findChild("div", {"class": "content"})

                matches = re.search(r'(?P<action>publiée|modifiée) le (?P<date>\d{2}\/\d{2}\/\d{4})', new.findChild("span", {"class": "date"}).text.strip())
                added_date = ""
                edited_date = ""
                if matches['action'] == "publiée":
                    added_date = matches["date"]
                elif matches['action'] == "modifiée":
                    edited_date = matches["date"]

                _LOGGER.debug("id: %s", id)
                _LOGGER.debug("url: %s", url)
                _LOGGER.debug("title: %s", title)
                _LOGGER.debug("content: %s", content)
                _LOGGER.debug("added_date: %s", added_date)
                _LOGGER.debug("edited_date: %s", edited_date)
        except Exception as e:
            _LOGGER.error("Unable to parse response.")
            _LOGGER.debug("Exception parsing response: %s", e)

    async def _async_get_page_content(self, log_errors=True):
        _LOGGER.debug("Updating from %s", self._url)
        try:
            return await self._async_client.request(
                "GET",
                self._url,
                timeout=self._timeout,
            )

        except httpx.RequestError as ex:
            if log_errors:
                _LOGGER.error(
                    "Error fetching form resource: %s failed with %s",
                    self._url,
                    ex,
                )