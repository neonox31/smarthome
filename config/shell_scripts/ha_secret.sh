#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Usage: ha_secret secret_key'
    exit 0
fi

grep ^$1: "/config/secrets.yaml" | sed 's/^[^:]*: //' | sed 's/ //g'
