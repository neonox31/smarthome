#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Usage: send_intercom_tts.sh message'
    exit 0
fi

message="${1}"

curl -sSf -H "Authorization: Bearer $(/config/shell_scripts/ha_secret.sh home_assistant_api_access_token)" -H "Content-Type: application/json" -d "{\"message\": \"${message}\", \"platform\": \"google_translate\"}" "$(/config/shell_scripts/ha_secret.sh home_assistant_internal_url)/api/tts_get_url" \
 | jq -r '.url' \
 | xargs curl \
 | ffmpeg -i - -c:a pcm_alaw -ac 1 -ar 8000 -sample_fmt s16 -f alaw -filter:a "volume=3.5" pipe:1 \
 | curl "http://$(/config/shell_scripts/ha_secret.sh intercom_user):$(/config/shell_scripts/ha_secret.sh intercom_password)@$(/config/shell_scripts/ha_secret.sh intercom_host)/cgi-bin/audio.cgi?action=postAudio&httptype=singlepart&channel=1" --header 'Content-Type: Audio/G.711A' --data-binary @- --max-time 3 || (($?==28))