#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Usage: send_chicken_coop_camera_command.sh command'
    exit 0
fi

base_url="$(/config/shell_scripts/ha_secret.sh camera_chicken_coop_control_url)"

case "${1}" in
   "up") command="command=0&onestep=1"
   ;;
   "down") command="command=2&onestep=1"
   ;;
   "left") command="command=4&onestep=1"
   ;;
   "right") command="command=6&onestep=1"
   ;;
   "preset1") command="command=31&onestep=0"
   ;;
   *) echo "Unknown camera command : ${1}" && exit 1
   ;;
esac

curl "${base_url}&${command}"




